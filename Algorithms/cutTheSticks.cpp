//https://www.hackerrank.com/challenges/cut-the-sticks/submissions/code/11943823

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void checkIfNumberOfSticksIsValid(const int val) {
    if (!(val >= 1 && val <= 1000)) {
        throw __FUNCTION__;
    }
}

void checkIfNumberSticksSizeIsValid(const int val) {
    if (!(val >= 1 && val <= 1000)) {
        throw __FUNCTION__;
    }
}


int getNumberOfSticks() {
    string tmp;
    getline(cin, tmp);
    int numberOfSticks= stoi(tmp);
    checkIfNumberOfSticksIsValid(numberOfSticks);
    return numberOfSticks;
}

vector<int> loadAllSticks(const int numberOfSticks) {
    vector<int> res;
    res.reserve(numberOfSticks + 1);
    int tmp;
    for (int i = 0 ; i < numberOfSticks ; ++i) {
        cin >> tmp;
        checkIfNumberSticksSizeIsValid(tmp); 
        res.push_back(tmp);
    }
    return res;
}
void print(vector<int>& sticks) {
    cout << sticks.size() << endl;  
    for (const auto& i : sticks) {
        cerr << i << " ";
    }
    cerr << endl;
}

void reduce(vector<int>& sticks) {
    const auto min = min_element(sticks.begin(), sticks.end());
    if (min == sticks.end()) return;
    
    cerr << "MIN IS :" << *min << endl;
    std::transform(sticks.begin(), sticks.end(), sticks.begin(), bind2nd(std::minus<int>(), *min));
    
    sticks.erase(
        remove_if(sticks.begin(), sticks.end(), bind2nd(std::less_equal<int>(), 0)),
        sticks.end());
}

int main() {
    try {
        const auto& numberOfSticks = getNumberOfSticks();
        auto sticks = loadAllSticks(numberOfSticks);
        while ( sticks.size() > 0 ) {
            print(sticks);
            reduce(sticks);
        }
    } catch (const char* error ) {
        cerr << " Got ERROR: " << error << endl;
        return -1;
    } catch (...) {
        cerr << " Got other ERROR";       
    }    
    return 0;
}
