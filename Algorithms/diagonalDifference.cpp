//https://www.hackerrank.com/challenges/diagonal-difference/submissions/code/12854136

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
using namespace std;

string getLineOrThrow() {
    string tmp;
    if (!getline(cin, tmp)) {
        throw "Read input error";
    }
    return tmp;
}

void checkIfMatrixSizeIsValid(const int val) {
    if (!(val >= 1 && val <= 100)) {
        throw __FUNCTION__;
    }
}

int getMatrixSize() {
    string tmp = getLineOrThrow();
    int matrixSize = stoi(tmp);
    checkIfMatrixSizeIsValid(matrixSize );
    return matrixSize ;
}

int main() {
    try {
        const auto& matrixSize = getMatrixSize();
        int firstLineSum = 0;
        int secondLineSum = 0;
        for (int i = 0 ; i < matrixSize; ++i) {
            stringstream matrixLine(getLineOrThrow());
            int tmp = 0;
            for (int j = 0 ; j <matrixSize; ++j) {
                matrixLine >> tmp;
                cerr << "(" << i << "," << j << ")" << endl;
                if (i == j) {
                    cerr << "ADD " << tmp << " to firstLineSum" << endl;
                    firstLineSum += tmp;                    
                } 
                if (i == matrixSize -1 -j) {
                    cerr << "ADD " << tmp << " to secondLineSum" << endl;
                    secondLineSum += tmp;
                }
            }
        }
        cerr << "1:" << firstLineSum << " 2:" << secondLineSum << endl;
        cout << abs(firstLineSum - secondLineSum) << endl;
    } catch (const char* error ) {
        cerr << " Got ERROR: " << error << endl;
        return -1;
    } catch (...) {
        cerr << " Got other ERROR";       
    }
        
    return 0;
}