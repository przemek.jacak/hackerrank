// https://www.hackerrank.com/challenges/library-fine/submissions/code/12897766

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <functional>
#include <stdexcept>

using namespace std;

namespace utils
{

string getLineOrThrow()
{
    string tmp;
    if (!getline(cin, tmp))
    {
        throw "Read input error";
    }
    return tmp;
}

void throwIfNumberIsNotInRage(const int val, const int min, const int max)
{
    if (!(val >= min && val <= max))
    {
        stringstream msg;
        msg << ": Value:" << val << " Is not in range [" << min << "," << max << "]";
        throw out_of_range(msg.str());
    }
}

int main(function<void()> fun)
{
    try
    {
        fun();
    }
    catch (const invalid_argument& error )
    {
        cerr << " Got INVALID_ARGUMENT ERROR: " << error.what() << endl;
        return -1;
    }
    catch (const out_of_range& error )
    {
        cerr << " Got OUT_OF_RANGE ERROR: " << error.what() << endl;
        return -2;
    }
    catch (const char* error )
    {
        cerr << " Got ERROR: " << error << endl;
        return -3;
    }
    catch (const string& error )
    {
        cerr << " Got ERROR: " << error << endl;
        return -4;
    }
    catch (...)
    {
        cerr << " Got some other ERROR";       
        return -5;
    }
    return 0;
}

}  // namespace utils

struct Date
{
    Date(): d(0), m(0), y(0) {}
    
    Date(const string& input)
    {
        stringstream buf(input);
        buf >> d;
        utils::throwIfNumberIsNotInRage(d, 1, 31);
        buf >> m;
        utils::throwIfNumberIsNotInRage(m, 1, 12);
        buf >> y;
        utils::throwIfNumberIsNotInRage(y, 1, 3000);
    }
    
    int d;
    int m;
    int y;
    
    Date operator-(const Date &other) const
    {
        Date res;
        res.d = d - other.d;
        res.m = m - other.m;
        res.y = y - other.y;
        
        return res;
    }
};

void libraryFine()
{
    Date actualReturnDate(utils::getLineOrThrow());
    Date expectedReturnDate(utils::getLineOrThrow());
    
    Date diff = actualReturnDate - expectedReturnDate;
    
    cerr << diff.d << " " << diff.m << " " << diff.y << endl;
    if (diff.y > 0)
    {
        cout << "10000" << endl;
        return;
    }
    
    if (diff.y < 0)
    {
        cout << "0" << endl;
        return;
    }
        
    if (diff.m > 0) 
    {
        cout << 500 * diff.m << endl;        
        return;
    }
    
    if (diff.m < 0) 
    {
        cout << "0" << endl;
        return;
    }
    
    if (diff.d > 0) 
    {
        cout << 15 * diff.d << endl;        
       return;
    }
    
    cout << "0" << endl;
}

int main() {
    return utils::main( [] { libraryFine(); });
}