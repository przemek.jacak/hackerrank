//https://www.hackerrank.com/challenges/find-digits/submissions/code/11984809

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

template <typename T>
void checkIfNumberIsValid(const T& val) {
    if (!(val > 0 && val < 10000000000)) {
        throw __FUNCTION__;
    }
}

template <typename T>
void checkIfNumberOfTestsValid(const T& val) {
    if (!(val >= 1 && val <= 15)) {
        throw __FUNCTION__;
    }
}

string getLineOrThrow() {
    string tmp;
    if (!getline(cin, tmp)) {
        throw "Read input error";
    }
    return tmp;
}

int getNumberOfTests() {
    int numberOfTests = stoi(getLineOrThrow());
    checkIfNumberOfTestsValid(numberOfTests);
    return numberOfTests;
}

void handleTest(const int number, const string& digits) {
    int res = 0;
    for (const auto& c : digits) {
        if (c == '0') continue;
        if ( number % ( c - '0') == 0) ++res;
    }
    cout << res << endl;
}

int main() {
    try {
    const auto numberOfTests = getNumberOfTests();
    for (int i = 0; i < numberOfTests; ++i) {
        const auto digits = getLineOrThrow();
        const unsigned long number = stol(digits);
        checkIfNumberIsValid(number);
        handleTest(number, digits);
    }
    } catch (const char* error ) {
        cerr << " Got ERROR: " << error << endl;
        return -1;
    } catch (...) {
        cerr << " Got other ERROR";       
    }
    return 0;
}