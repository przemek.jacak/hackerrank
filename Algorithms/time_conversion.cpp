// Submitted https://www.hackerrank.com/challenges/time-conversion/submissions/code/13018659

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <functional>
#include <stdexcept>

using namespace std;

namespace utils
{

string getLineOrThrow()
{
    string tmp;
    if (!getline(cin, tmp))
    {
        throw "Read input error";
    }
    return tmp;
}
    
int main(function<void()> fun)
{
    try
    {
        fun();
    }
    catch (const invalid_argument& error )
    {
        cerr << " Got INVALID_ARGUMENT ERROR: " << error.what() << endl;
        return -1;
    }
    catch (const out_of_range& error )
    {
        cerr << " Got OUT_OF_RANGE ERROR: " << error.what() << endl;
        return -2;
    }
    catch (const char* error )
    {
        cerr << " Got ERROR: " << error << endl;
        return -3;
    }
    catch (const string& error )
    {
        cerr << " Got ERROR: " << error << endl;
        return -4;
    }
    catch (...)
    {
        cerr << " Got some other ERROR";       
        return -5;
    }
    return 0;
}

}  // namespace utils
void throwExceptionOnInvalidInput(const std::string& input)
{
    if (input.size() != 10) throw invalid_argument("Invalid input size (should be 10)");  
}

bool isPm(const std::string& input)
{
    if (input[input.size() - 2] == 'P' && input[input.size() - 1] == 'M')
        return true;
    else if (input[input.size() - 2] == 'A' && input[input.size() - 1] == 'M')
        return false;
    throw invalid_argument("Missing PM/AM suffix");
}

int getHours(const std::string& input, const bool isPm)
{
    int res = 0;
    stringstream ss;
    ss << input[0];
    ss << input[1];
    ss >> res;
    if (!(res >=1 && res <= 12)) 
        throw out_of_range("hour should be in range [1,12]");
    
    if (isPm)
    {
        if (res !=12)
        {
            res +=12;
        }
    }
    else if (res == 12)
    {
        res = 0;
    }
    return res;
}

int getMinutes(const std::string& input)
{
    int res = 0;
    
    stringstream ss;
    ss << input[3];
    ss << input[4];
    ss >> res;
    if (!(res >=0 && res <= 59)) 
        throw out_of_range("minute should be in range [0,59]");
    
    return res;
}

int getSeconds(const std::string& input)
{
    int res = 0;
    
    stringstream ss;
    ss << input[6];
    ss << input[7];
    ss >> res;
    if (!(res >=0 && res <= 59)) 
        throw out_of_range("second should be in range [0,59]");
    
    return res;
}

string addZeroIfNeeded(const int value)
{
    stringstream ss;
    if (value < 10)
        ss << "0";
    ss << value;
    return ss.str();
}

void time_conversion()
{
    auto inputLine = utils::getLineOrThrow();
    
    throwExceptionOnInvalidInput(inputLine);
    string hours   = addZeroIfNeeded(getHours(inputLine, isPm(inputLine)));
    string minutes = addZeroIfNeeded(getMinutes(inputLine));
    string seconds = addZeroIfNeeded(getSeconds(inputLine));
    
    cout << hours << ":" << minutes << ":" << seconds << endl;
}

int main() {
    return utils::main( [] { time_conversion(); });
}