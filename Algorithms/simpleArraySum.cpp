#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

string getLineOrThrow() {
    string tmp;
    if (!getline(cin, tmp)) {
        throw "Read input error";
    }
    return tmp;
}

int getNumberOfTests() {
    int numberOfTests = stoi(getLineOrThrow());
    return numberOfTests;
}

int main() {
    int size = getNumberOfTests();
    
    long long int result = 0;
    int tmp = 0;
    for (int i =0; i < size; i++) {
        cin >> tmp;
        result += tmp;
    }
    
    std::cout << result <<std::endl;
    
    return 0;
}
