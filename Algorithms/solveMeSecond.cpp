//https://www.hackerrank.com/challenges/solve-me-second/submissions/code/11849933

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
using namespace std;

template<typename A, typename B>
auto solveMe(A a, B b) -> decltype(a + b) {
  return a+b;
}

void handleLine(const string& line) {
    stringstream ss(line);
    
    float num1, num2;
    ss >> num1 >> num2;
    cout << solveMe(num1,num2) << endl;
}

int main() {
    string line;
    getline(cin, line);
    
    int t = stoi(line);
    
    if (t < 0) {
        return 0;
    }
    
    for (int i=0; i<t; i++) {
        if (getline(cin, line)) {
            handleLine(line);
        }
    }

    return 0;
}