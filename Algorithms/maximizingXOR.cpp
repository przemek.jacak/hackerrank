//https://www.hackerrank.com/challenges/maximizing-xor/submissions/code/11861771

#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <cstdlib>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;
/*
 * Complete the function below.
 */
int maxXor(int l, int r) {
    int res = 0;
    for (int i = l ; i <= r ; ++i)
        for (int j = i ; j <= r ; ++j) {
            res = max (res, i ^ j );
        }
    return res;
}

bool isLvalid(const int l) {
    return (l >= 1 && l <= 1000);
}

bool isRvalid(const int l, const int r) {
    return (l >= 1 && l <= r && l <= 1000);
}

int main() {
    string line;
    getline(cin, line);
    int _l = stoi(line);
    
    if (!isLvalid(_l)) {
//        cout << " L invalid" << endl;
        return 0;
    }
    
    getline(cin, line);
    int _r = stoi(line);
    
    if (!isRvalid(_l, _r)) {
//        cout << " R invalid" << endl;
        return 0;
    }
    
    cout << maxXor(_l, _r);
    
    return 0;
}
