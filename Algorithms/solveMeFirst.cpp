//https://www.hackerrank.com/challenges/solve-me-first/submissions/code/11849387
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

template<typename A, typename B>
auto solveMeFirst(A a, B b) -> decltype(a + b) {
  return a+b;
}

int main() {
    float num1, num2;
    cin>>num1>>num2;
    
    cout << solveMeFirst(num1,num2);
    return 0;
}
