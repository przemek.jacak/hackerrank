//https://www.hackerrank.com/challenges/caesar-cipher-1/submissions/code/13096305
//https://www.hackerrank.com/challenges/caesar-cipher-1/submissions/code/13096908

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <functional>
#include <stdexcept>

using namespace std;
namespace utils
{
string getLineOrThrow()
{
    string tmp;
    if (!getline(cin, tmp))
    {
        throw "Read input error";
    }
    return tmp;
}

void throwIfNumberIsNotInRage(const int val, const int min, const int max)
{
    if (!(val >= min && val <= max))
    {
        stringstream msg;
        msg << ": Value:" << val << " Is not in range [" << min << "," << max << "]";
        throw out_of_range(msg.str());
    }
}
    
int getValidNumberFromLine(const string& input, const int min, const int max)
{
    int res = stoi(input);
    throwIfNumberIsNotInRage(res, min, max);
    return res;
}
    
int main(function<void()> fun)
{
    try
    {
        fun();
    }
    catch (const invalid_argument& error )
    {
        cerr << " Got INVALID_ARGUMENT ERROR: " << error.what() << endl;
        return -1;
    }
    catch (const out_of_range& error )
    {
        cerr << " Got OUT_OF_RANGE ERROR: " << error.what() << endl;
        return -2;
    }
    catch (const char* error )
    {
        cerr << " Got ERROR: " << error << endl;
        return -3;
    }
    catch (const string& error )
    {
        cerr << " Got ERROR: " << error << endl;
        return -4;
    }
    catch (...)
    {
        cerr << " Got some other ERROR";       
        return -5;
    }
    return 0;
}
}

static const char upperMin = 65;
static const char upperMax = 90;
static const char lowerMin = 97;
static const char lowerMax = 122;

char addKey(const char& input, const char& key)
{
    char base = 0;
    
    if (islower(input)) base = lowerMin;
    else if (isupper(input)) base = upperMin;
    else throw logic_error("This should not happen");
    
    return base + (((input - base) + key ) % 26);
}
    
void encryptAndPrint(const char& input, const char& key)
{
    char res = input;
    
    if (isalpha(input))
        res = addKey(input, key);
    
    cerr << "input: " << (int)input << "'" << input << "' -> iutput:" << (int)res << "'" << res <<"'" << endl;
    cout << res;
}
    
void caesarCipher()
{
    utils::getLineOrThrow(); // We do not care about input size
    const string inputLine = utils::getLineOrThrow();
    const char key = (char)utils::getValidNumberFromLine(utils::getLineOrThrow(), 0, 100);
    
    cerr << "Input:'" << inputLine << "'" << endl;
    cerr << "Key:'" << (int)key << "'" << endl;
    
    for_each(inputLine.begin(), inputLine.end(), [&key](const char& n){encryptAndPrint(n, key);});
    
    cout << endl;
}
    
int main() {
    return utils::main( [] {caesarCipher();});
}