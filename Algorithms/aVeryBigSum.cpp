//https://www.hackerrank.com/challenges/a-very-big-sum/submissions/code/12204291

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
using namespace std;

template <typename T>
void checkIfNumbersCountIsValid(const T& val) {
    if (!(val >= 1 && val <= 10)) {
        throw __FUNCTION__;
    }
}

string getLineOrThrow() {
    string tmp;
    if (!getline(cin, tmp)) {
        throw "Read input error";
    }
    return tmp;
}

int getNumbersCount() {
    int numbersCount = stoi(getLineOrThrow());
    checkIfNumbersCountIsValid(numbersCount);
    return numbersCount;
}

int main() {
    try {
        const auto numbersCount= getNumbersCount();
        stringstream input(getLineOrThrow());
        
        unsigned int number = 0;       
        unsigned long long sum = 0;
        int i = 0;
        while (i < numbersCount) {
            input >> number;
            sum += number;
            ++i;
        }
        cout << sum << endl;
    } catch (const char* error ) {
        cerr << " Got ERROR: " << error << endl;
        return -1;
    } catch (...) {
        cerr << " Got other ERROR";       
    }
    return 0;    
}