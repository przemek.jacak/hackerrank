//https://www.hackerrank.com/challenges/staircase/submissions/code/12878388

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
using namespace std;

namespace utils
{
    
string getLineOrThrow()
{
    string tmp;
    if (!getline(cin, tmp))
    {
        throw "Read input error";
    }
    return tmp;
}

void throwIfNumberIsNotInRage(const int val, const int min, const int max)
{
    if (!(val >= min && val <= max))
    {
        stringstream msg;
        msg << ": Value:" << val << " Is not in range [" << min << "," << max << "]";
        throw out_of_range(msg.str());
    }
}

int getNumberFromLineOrThrowIfInvalid(const int min, const int max)
{
    int res = stoi(utils::getLineOrThrow());
    utils::throwIfNumberIsNotInRage(res, min, max);
    return res;   
}

int main(function<void()> fun)
{
    try
    {
        fun();
    }
    catch (const invalid_argument& error )
    {
        cerr << " Got INVALID_ARGUMENT ERROR: " << error.what() << endl;
        return -1;
    }
    catch (const out_of_range& error )
    {
        cerr << " Got OUT_OF_RANGE ERROR: " << error.what() << endl;
        return -2;
    }
    catch (const char* error )
    {
        cerr << " Got ERROR: " << error << endl;
        return -3;
    }
    catch (const string& error )
    {
        cerr << " Got ERROR: " << error << endl;
        return -4;
    }
    catch (...)
    {
        cerr << " Got some other ERROR";       
        return -5;
    }
    return 0;
}

}  // namespace utils

int getHeight() {
    return utils::getNumberFromLineOrThrowIfInvalid(1, 100);
}

void staircase()
{
    int height = getHeight();
    
    string buf;
    for (int i =0; i < height; ++i) buf.append(" ");
    
    while (height > 0) {
        buf[height-1] = '#';
        cout << buf << endl;
        --height;
    }
}

int main() {
    return utils::main([] {staircase();});
}
