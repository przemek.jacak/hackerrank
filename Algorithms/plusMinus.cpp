//https://www.hackerrank.com/challenges/plus-minus/submissions/code/12875121

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
using namespace std;

string getLineOrThrow() {
    string tmp;
    if (!getline(cin, tmp)) {
        throw "Read input error";
    }
    return tmp;
}

void checkIfTotalCountIsValid(const int val) {
    if (!(val >= 1 && val <= 100)) {
        throw __FUNCTION__;
    }
}

void validateNumber(const int val) {
    if (!(val >= -100 && val <= 100)) {
        throw __FUNCTION__;
    }
}

int getTotalCount() {
    string tmp = getLineOrThrow();
    int matrixSize = stoi(tmp);
    checkIfTotalCountIsValid(matrixSize );
    return matrixSize ;
}

int main() {
    try {
        int totalCount = getTotalCount();
        int positives = 0;
        int negatives = 0;
        int zeros = 0;
        stringstream line(getLineOrThrow());
        int tmp = 0;
        for ( int i = 0; i < totalCount; ++i) {
            line >> tmp;
            validateNumber(tmp);
            if (tmp == 0) zeros++;
            else if (tmp < 0) negatives++;
            else positives++;
        }
        cout.precision(3);
        cout << float(positives) / float(totalCount) << endl;
        cout << float(negatives) / float(totalCount) << endl;
        cout << float(zeros) / float(totalCount) << endl;
        
        
    } catch (const char* error ) {
        cerr << " Got ERROR: " << error << endl;
        return -1;
    } catch (...) {
        cerr << " Got other ERROR";       
    }
    return 0;
}