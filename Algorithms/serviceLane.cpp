//https://www.hackerrank.com/challenges/service-lane/submissions/code/11874349

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void checkIfHighwayLengthValid(const int val) {
    if (!(val >= 2 && val <= 100000)) {
        throw 1;
    }
}

void checkIfNumberOfTestsValid(const int val) {
    if (!(val >= 2 && val <= 100000)) {
        throw 2;
    }
}

void checkIfLineValid(const int line) {
    if  (!(line >= 1 && line <= 3)) {
        throw 3;
    }
}

vector<int> loadHighway(const int highwayLength) {
    vector<int> res;
    res.reserve(highwayLength + 1);
    
    int tmp;
    for ( int i = 0 ; i < highwayLength; ++i) {
        cin >> tmp;
        checkIfLineValid(tmp); 
        res.emplace_back(tmp);
    }
    
    return res;
}

void checkIfTestParamsAreValid(const int start, const int stop, const size_t size) {
    if ( !(0 <= start && start < stop &&  stop < size) ) {
        throw 4;
    }
}

void handleTest(const vector<int>& highway) {
    int startPoint;
    int endPoint;
    cin >> startPoint >> endPoint;
    checkIfTestParamsAreValid(startPoint, endPoint, highway.size());
    
    auto begin = highway.begin() + startPoint;
    auto end = highway.begin() + endPoint;
    auto min = min_element(begin, end + 1);   
    cout << *min << endl;
}
    
int main() {
    int highwayLength;
    int numberOfTests;
    cin >> highwayLength >> numberOfTests;
    
    try {
        checkIfHighwayLengthValid(highwayLength);
        checkIfNumberOfTestsValid(numberOfTests);
        const auto highway = loadHighway(highwayLength);
        for (int i = 0 ; i < numberOfTests; ++i) {
            handleTest(highway);
        }
    } catch (const int& error ) {
        cerr << " Got ERROR: " << error << endl;
        return -1;
    }
    
    return 0;
}
