//https://www.hackerrank.com/challenges/the-love-letter-mystery/submissions/code/11910989

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int correctChar(const string& buf, const int pos) {
    return abs(buf[pos] - buf[buf.size() - 1 - pos]);
}

void handleTest() {
    string tmp;
    if(!getline(cin, tmp)) {
        throw "Error reading input";        
    }
        
    int numberOfChanges = 0;
    for ( int i = 0 ; i < tmp.size()/2; ++i) {
        numberOfChanges += correctChar(tmp, i);
    }
    cout << numberOfChanges << endl;
}

void checkIfNumberOfTestsValid(const int val) {
    if (!(val >= 1 && val <= 10)) {
        throw __FUNCTION__;
    }
}

int getNumberOfTests() {
    string tmp;
    getline(cin, tmp);
    int numberOfTests = stoi(tmp);
    checkIfNumberOfTestsValid(numberOfTests);
    return numberOfTests;
}

int main() {
    try {
        const auto& numberOfTests = getNumberOfTests();
        for (int i = 0 ; i < numberOfTests; ++i) {
            handleTest();
        }
    } catch (const char* error ) {
        cerr << " Got ERROR: " << error << endl;
        return -1;
    } catch (...) {
        cerr << " Got other ERROR";       
    }
    
        
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    return 0;
}