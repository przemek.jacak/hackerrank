//https://www.hackerrank.com/challenges/utopian-tree/submissions/code/11860760

#include <iostream>
using namespace std;

bool valueIsCorrect(int val) {
    return (val >= 0 && val <= 60); 
}

void handleLine(const string& line) {
    try {
        int val = stoi(line);
        if (!valueIsCorrect(val)) {
            return;
        }
        unsigned int res = 1;
        for (int i = 0 ; i < val; ++i) {
            res = (i%2) ? (res + 1) : (res * 2);
        }
        cout << res << endl;
    } catch ( ... ) {
        
    }
}

bool numberOfRecordsIsCorrect(int recordsCount) {
    return (recordsCount >= 1 && recordsCount <= 10);
}

int main() {
    string line;
    getline(cin, line);
    
    int t = stoi(line);
    
    if (!numberOfRecordsIsCorrect(t)) {
        return 0;
    }
    
    for (int i=0; i<t; i++) {
        if (getline(cin, line)) {
            handleLine(line);
        }
    }

    return 0;
}