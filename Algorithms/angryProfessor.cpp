//https://www.hackerrank.com/challenges/angry-professor/submissions/code/12437463

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>
using namespace std;

void checkIfNumberOfTestsValid(const int val) {
    if (!(val >= 1 && val <= 10)) {
        throw __FUNCTION__;
    }
}

int getNumberOfTests() {
    string tmp;
    getline(cin, tmp);
    int numberOfTests = stoi(tmp);
    checkIfNumberOfTestsValid(numberOfTests);
    return numberOfTests;
}

string getLineOrThrow() {
    string tmp;
    if (!getline(cin, tmp)) {
        throw "Read input error";
    }
    return tmp;
}
    
void getTestParams(int& classSize, int& minimumSize) {
    string tmp = getLineOrThrow();
    stringstream buf (tmp);
    
    buf >> classSize;
    buf >> minimumSize;
    
    cerr << classSize << ":" << minimumSize << endl;
}

void handleTest() {
    int classSize = 0;
    int minimumSize = 0;
    
    getTestParams(classSize, minimumSize);
    
    int onTime = 0;
    int late = 0;
    stringstream arrivals(getLineOrThrow());
    
    int tmp = 0;
    for (int i = 0; i < classSize; ++i) {
        arrivals >> tmp;
        
        if (tmp <= 0) ++onTime;
        else if (tmp > 0) ++late;
    }
    
    if (onTime < minimumSize) cout << "YES";
    else cout << "NO";
    
    cout <<endl;
    
    cerr << "onTime: " << onTime << " late:" << late << endl;
}

int main() {
    try {
        const auto& numberOfTests = getNumberOfTests();
        for (int i = 0 ; i < numberOfTests; ++i) {
            handleTest();
        }
    } catch (const char* error ) {
        cerr << " Got ERROR: " << error << endl;
        return -1;
    } catch (...) {
        cerr << " Got other ERROR";       
    }
        
    return 0;
}
