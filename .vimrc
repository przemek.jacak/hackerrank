set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" C-d
Plugin 'scrooloose/nerdtree'

" C-p C-n
Plugin 'vim-scripts/YankRing.vim'

"F8
Plugin 'majutsushi/tagbar'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
filetype plugin on

" tabs
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set smarttab

" splits navigation
nnoremap <C-down> <C-W><C-J>
nnoremap <C-up> <C-W><C-K>
nnoremap <C-right> <C-W><C-L>
nnoremap <C-left> <C-W><C-H>

" NerdTree
map <C-d> :NERDTreeToggle<CR>

"Tags
nmap <F8> :TagbarToggle<CR>

