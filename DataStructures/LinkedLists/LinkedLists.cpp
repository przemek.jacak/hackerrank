Node* createNode(const int data)
{
    auto node = new Node();
    node->next = 0;
    node->data = data;
    return node;
}

Node* goToPosition(Node *head, int position)
{
    auto item = head;
    while ( position > 0 && item)
    {
        item = item->next;
        position--;
    }
    return item;
}

Node* goToTail(Node* head)
{
    while (head && head->next)
    {
        head = head->next;  
    }
    return head;
}

Node* insertBack(Node* head,int data)
{
    auto node = createNode(data);
    auto tail = goToTail(head);
    if (tail) 
    {
        tail->next = node;
    }
    else
    {
        head = node;
    }
    return head;
}

Node* insertFront(Node *head,int data)
{
    auto node = createNode(data);
    node->next = head;
    return node;
}

Node* insertNth(Node *head, int data, int position)
{
    if (position == 0 )
    {
        return insertFront(head, data);     
    }
    
    auto node = head;
    for (int i = 0; i < position-1; i++)
    {
        if (!node->next) throw out_of_range("Invalid position");
        node = node->next;
    }
    auto newNode = createNode(data);
    newNode->next = node->next;
    node->next = newNode;
    
    return head;
}

Node* delereFirst(Node* head)
{
    auto next = head->next;
    delete head;
    return next;
} 

Node* deleteAt(Node *head, int position)
{
    if (position == 0)
    {
        return delereFirst(head);
    }
    
    auto itemBefore = goToPosition(head, position - 1);    
    auto itemToDelete = itemBefore->next;
    itemBefore->next = itemToDelete->next;
    delete itemToDelete;
    
    return head;
}

int compareLists(Node *headA, Node* headB)
{
    while (headA && headB)
    {
        if (headA->data != headB->data) return 0;
        headA = headA->next;
        headB =headB->next;
    }
    if (headA != headB) return 0;
    return 1;
}

void printAll(Node *head)
{
    while (head) {
        std::cout << head->data << std::endl;
        head = head->next;  
    }
}

void reversePrint(Node *head)
{
    string res = "";
    while (head) 
    {
        res = to_string(head->data) + "\n" + res;
        head = head->next;
    }
    cout << res;
}