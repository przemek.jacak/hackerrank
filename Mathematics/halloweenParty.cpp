//https://www.hackerrank.com/challenges/halloween-party/submissions/code/11977988

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

void checkIfNumberOfTestsValid(const int val) {
    if (!(val >= 1 && val <= 10)) {
        throw __FUNCTION__;
    }
}

string getLineOrThrow() {
    string tmp;
    if (!getline(cin, tmp)) {
        throw "Read input error";
    }
    return tmp;
}

int getNumberOfTests() {
    int numberOfTests = stoi(getLineOrThrow());
    checkIfNumberOfTestsValid(numberOfTests);
    return numberOfTests;
}

void checkIfNumberOfCutsValid(const int val) {
    if (!(val >= 2 && val <= 10000000)) {
        throw __FUNCTION__;
    }
}
    
int getNumberOfCuts() {
    int res = stoi(getLineOrThrow());
    checkIfNumberOfCutsValid(res);
    return res;
}

int main() {
    const auto numberOfTests = getNumberOfTests();
    for (int i = 0; i < numberOfTests; ++i) {
        const auto input = getNumberOfCuts();
        unsigned long rows = input / 2;
        unsigned long columns = input - rows;
        cerr << rows << " : " << columns << endl;
        cout << rows * columns << endl;
    }
    return 0;
}
